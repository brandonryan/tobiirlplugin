#pragma once

#include <ShlObj_core.h>
#include <string>
#include <filesystem>

//TODO: cant log to cvar manager from here. Would be nice to figure out how to get a hold of the logger
namespace TobiiRL {
	std::string wtostring(std::wstring w) {
		return std::string(w.begin(), w.end());
	}

	std::wstring getReplayPath() {
		wchar_t* wpath;
		if (SHGetKnownFolderPath(FOLDERID_Documents, 0, nullptr, &wpath) != S_OK) {
			//dont know why this would ever error out but whatever
			return L"";
		}
		//copy into a datatype thats a bit more manageable...
		auto replaysPath = std::wstring(wpath) + L"\\My Games\\Rocket League\\TAGame\\Demos";
		//free the memory
		CoTaskMemFree(wpath);
		return replaysPath;
	}

	std::wstring getNewestReplayID() {
		using namespace std::filesystem;
		directory_entry latestEntry = directory_entry();
		for (auto entry : directory_iterator(getReplayPath())) {
			if (!entry.is_regular_file()) {
				continue;
			}
			if (!latestEntry.exists() || entry.last_write_time() > latestEntry.last_write_time()) {
				latestEntry = entry;
			}
		}
		auto filename = latestEntry.path().filename().wstring();
		return filename.substr(0, filename.find('.'));
	}
}