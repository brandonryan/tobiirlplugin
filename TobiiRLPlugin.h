#pragma once
#pragma comment(lib, "pluginsdk.lib")
#pragma comment(lib, "tobii_stream_engine.lib")

//TODO: break out includes that are only needed in the source to the cpp file
#include "bakkesmod/plugin/bakkesmodplugin.h"
#include <tobii/tobii.h>
#include <tobii/tobii_streams.h>

#include "ReplayEyeData.h"

namespace TobiiRL {
	typedef struct EyePointData {
		float microseconds;
		float x, y; //TODO int
	} EyePointData;

	void processTobiiGazeData(tobii_gaze_point_t const* gp, void* ctx);

	class Plugin : public BakkesMod::Plugin::BakkesModPlugin {
	private:
		bool recording = false;
		bool paused = false;

		std::shared_ptr<ReplayEyeData> data = nullptr;

		//used for playback loop
		std::vector<ReplayGazePoint> gazeData;
		int playbackIndex;

		tobii_api_t* tobii = nullptr;
		tobii_device_t* tobiiDevice = nullptr;
		std::vector<std::string> tobiiDeviceURLs;

	public:
		virtual void onLoad();
		virtual void onUnload();
		void Render(CanvasWrapper canvas);

	private:
		void initTobii();
		void initHooks();
		void refreshTobiiDevices();
		void connectTobiiDevice(std::string url);

		void onReplaySave();
		void startRecording();
		void stopRecording();
		void OnReplayLoad();
		void OnReplayUnload();
		void updateViewTarget();

		void pollTobiiData();
		void disconnectTobiiDevice();
		bool isTobiiError(tobii_error_t err, std::string msg);
	};
};