#pragma once

#include <string>
#include <vector>
#include <cstddef>
#include <map>
#include <chrono>

#include <cereal/cereal.hpp>
#include <cereal/access.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/chrono.hpp>

#include "bakkesmod/wrappers/wrapperstructs.h"


namespace TobiiRL {
	typedef struct ReplayGazePoint {
		float useconds;
		float x, y;

		template<class Archive>
		void serialize(Archive& archive, uint32_t version)
		{
			archive(useconds, x, y);
		}
	} ReplayGazePoint;

	typedef struct PlayerData {
		std::vector<ReplayGazePoint> gazeData;

		template<class Archive>
		void serialize(Archive& archive, uint32_t version)
		{
			archive(gazeData);
		}
	} PlayerData;

	class ReplayEyeData {
	private:
		std::chrono::system_clock::time_point epochAnchor;
		std::map<unsigned long long, PlayerData> playerData;

		friend class cereal::access;
		template<class Archive>
		void serialize(Archive& archive, uint32_t version) {
			archive(epochAnchor, playerData);
		}
	public:
		//used to add an offset to written timestamps
		long long usecsOffset = 0;

		ReplayEyeData();
		std::vector<ReplayGazePoint> getPlayerGazeData(unsigned long long steamId);
		void Write(unsigned long long steamId, float x, float y, float time);
		void Save(std::wstring path);
	};

	std::shared_ptr<ReplayEyeData> LoadReplayEyeData(std::wstring path);
}

//This needs to be updated every time the type is changed
CEREAL_CLASS_VERSION(TobiiRL::ReplayEyeData, 0);
CEREAL_CLASS_VERSION(TobiiRL::PlayerData, 0);
CEREAL_CLASS_VERSION(TobiiRL::ReplayGazePoint, 0);