#include "ReplayEyeData.h"

#include <cereal/archives/portable_binary.hpp>
#include <fstream>
#include <ShlObj_core.h>

namespace TobiiRL {
	std::shared_ptr<ReplayEyeData> LoadReplayEyeData(std::wstring filePath) {
		std::ifstream in_stream(filePath, std::ios::binary);
		auto data = std::make_shared<ReplayEyeData>();

		if(INVALID_FILE_ATTRIBUTES != GetFileAttributesW(filePath.c_str())) {
			cereal::PortableBinaryInputArchive archive(in_stream);
			archive(*data);
			in_stream.close();
		}
		
		return data;
	}

	ReplayEyeData::ReplayEyeData() {
		using namespace std::chrono;
		auto anchorTime = system_clock::now();
		this->epochAnchor = anchorTime;
	}

	std::vector<ReplayGazePoint> ReplayEyeData::getPlayerGazeData(unsigned long long steamId) {
		if (this->playerData.count(steamId) == 0) {
			return std::vector<ReplayGazePoint>();
		} else {
			return this->playerData[steamId].gazeData;
		}
	}

	void ReplayEyeData::Write(unsigned long long steamId, float x, float y, float time) {
		this->playerData[steamId].gazeData.push_back({
			time,
			x, y
		});
	}

	void ReplayEyeData::Save(std::wstring filePath) {
		//TODO: sort the array before saving
		std::ofstream out_stream(filePath, std::ios::binary);
		cereal::PortableBinaryOutputArchive archive(out_stream);
		archive(*this);
		out_stream.close();
	}
}