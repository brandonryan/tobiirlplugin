#include "TobiiRLPlugin.h"
#include "util.hpp"

#include <filesystem>

BAKKESMOD_PLUGIN(TobiiRL::Plugin, "Tobii Replay Eye Tracking", "0.3", PLUGINTYPE_REPLAY)

#define NULL_CHECK(wrapper) if(wrapper.IsNull()) return

const std::wstring EYE_DATA_FOLDER_NAME = L"Eye Data";
const std::wstring EYE_DATA_EXT = L".eyedata";

const std::string CAMERA_STATE_BALL_CAM = "CameraState_BallCam_TA";
const std::string CAMERA_STATE_CAR_CAM = "CameraState_Car_TA";

constexpr float REPLAY_OFFSET = .15;

/*
Notes:
Null checking:
1) .isNull on anything from the wrappers
2) if no .isNull wrapper.memory_address == nullptr check
3) if its just a struct, should be fine.

Look in header files in bakkesmodsdk dir to find wrappers to use with HookEventWithCaller when you want to get data out of the callbacks

*/ 

//TODO: get a liscence for analytical use

namespace TobiiRL {
	//TODO: create the EyeTracking folder
	void Plugin::onLoad() {
		this->initTobii();
		this->initHooks();
		
		//TODO: init cvars
		//TODO: refresh plugin settings

		//Create replay eye data directory
		if (std::filesystem::create_directory(getReplayPath() + L"\\" + EYE_DATA_FOLDER_NAME)) {
			cvarManager->log("Created replay eye data directory");
		}

		cvarManager->log("Tobii RL Plugin Initialized");
	}

	//Events
	void Plugin::initHooks() {
		//Offline match start
		gameWrapper->HookEventPost("Function TAGame.GameEvent_TA.EventMatchStarted", std::bind(&Plugin::startRecording, this));
		//Online match start
		gameWrapper->HookEventPost("Function OnlineSubsystemSteamworks.OnlineSubsystemSteamworks.OnlineMatchStart", std::bind(&Plugin::startRecording, this));
		//Match end
		gameWrapper->HookEventPost("Function TAGame.GameEvent_Soccar_TA.OnMatchEnded", std::bind(&Plugin::stopRecording, this));
		//Leave match
		gameWrapper->HookEventPost("Function TAGame.GFxShell_TA.LeaveMatch", std::bind(&Plugin::stopRecording, this));

		//On replay save
		gameWrapper->HookEventPost("Function TAGame.GFxData_ReplayManager_TA.SaveReplay", std::bind(&Plugin::onReplaySave, this));
		//On replay load replay load
		gameWrapper->HookEventPost("Function TAGame.GameInfo_Replay_TA.EventGameEventSet", std::bind(&Plugin::OnReplayLoad, this));
		//On replay leave
		gameWrapper->HookEventPost("Function TAGame.GFxHUD_Replay_TA.Destroyed", std::bind(&Plugin::OnReplayUnload, this));
		//On replay deleted
		gameWrapper->HookEventWithCallerPost<ActorWrapper>("Function TAGame.ReplayManager_TA.DeleteReplayFile", [this](ActorWrapper wrapper, void* params, std::string ev) {
			// *Bakkes magic shake and bake formula*
			struct DeleteReplayParams {
				uintptr_t replayName;
			};
			DeleteReplayParams* p = reinterpret_cast<DeleteReplayParams*>(params);
			std::wstring replayName = UnrealStringWrapper(reinterpret_cast<uintptr_t>(&p->replayName)).ToWideString();
			std::filesystem::path path = getReplayPath() + L"\\" + EYE_DATA_FOLDER_NAME + L"\\" + replayName + EYE_DATA_EXT;
			if (std::filesystem::remove(path)) {
				cvarManager->log("Replay eye data " + std::string(replayName.begin(), replayName.end()) + " deleted");
			}
		});

		//Camera target changed
		gameWrapper->HookEventPost("Function TAGame.Camera_TA.OnViewTargetChanged", std::bind(&Plugin::updateViewTarget, this));
		//Game tick for polling
		gameWrapper->HookEventPost("Function Engine.GameViewportClient.Tick", std::bind(&Plugin::pollTobiiData, this));
	}

	void Plugin::onUnload() {
		if (this->tobiiDevice != nullptr) {
			cvarManager->log("device is null");
			isTobiiError(tobii_device_destroy(this->tobiiDevice), "Could not release tobii device object.");
		}
		
		isTobiiError(tobii_api_destroy(this->tobii), "Could not release tobii api.");
	}

	void Plugin::startRecording() {
		//need a check to make sure there is no split screen going on, but it doesnt seem doable (bakkes said)
		cvarManager->log("Starting Recording");
		this->data = std::make_shared<ReplayEyeData>();

		if (this->tobiiDevice == nullptr || this->recording) {
			return;
		}
		
		//TODO: figure out why data is coming back off sync from this maybe?
		tobii_error_t err = tobii_gaze_point_subscribe(this->tobiiDevice, [](tobii_gaze_point_t const* gp, void* ctx) {
			//this is a fucking mess because tobii uses raw function pointers. have to pull "this" out of context.
			Plugin* self = static_cast<Plugin*>(ctx);

			//invalid = out of bounds
			if (gp->validity == TOBII_VALIDITY_INVALID) {
				return;
			}

			auto onlineGame = self->gameWrapper->GetOnlineGame();
			NULL_CHECK(onlineGame);

			self->data->Write(self->gameWrapper->GetSteamID(), gp->position_xy[0], gp->position_xy[1], onlineGame.GetSecondsElapsed());
		}, this);

		if (!isTobiiError(err, "Could not subscribe to point data.")) {
			this->recording = true;
		}
	}

	void Plugin::stopRecording() {
		cvarManager->log("Stopping Recording");
		if (!this->recording) {
			return;
		}
		this->recording = false;
		if (this->tobiiDevice != nullptr) {
			isTobiiError(tobii_gaze_point_unsubscribe(this->tobiiDevice), "Could not unsubscribe from point data");
		}
	}

	void Plugin::onReplaySave() {
		cvarManager->log("Saving Replay");

		if (!this->recording) {
			stopRecording();
		}

		//give the game 1 second to save the replay... There might be a better way to make sure, but my hard drive is shit and it manages. *shrug*
		gameWrapper->SetTimeout([this](GameWrapper* gw) {
			cvarManager->log("Saving gaze data");

			const std::wstring data_file_name = getReplayPath() + L"\\" + EYE_DATA_FOLDER_NAME + L"\\" + getNewestReplayID() + EYE_DATA_EXT;
			cvarManager->log("Writing gaze data to:" + wtostring(data_file_name));
			this->data->Save(data_file_name);
		}, 1);
	}

	void Plugin::OnReplayLoad() {
		std::wstring replayName = gameWrapper->GetGameEventAsReplay().GetReplay().GetFilename().ToWideString();
		std::wstring eyeDataPath = getReplayPath() + L"\\" + EYE_DATA_FOLDER_NAME + L"\\" + replayName + EYE_DATA_EXT;

		cvarManager->log("Loading: " + std::string(eyeDataPath.begin(), eyeDataPath.end()));

		this->data = LoadReplayEyeData(eyeDataPath);
		cvarManager->log("Eye data file loaded sucessfully");

		//Render for replay drawing
		gameWrapper->RegisterDrawable(std::bind(&Plugin::Render, this, std::placeholders::_1));
	}

	void Plugin::OnReplayUnload() {
		this->gameWrapper->UnregisterDrawables();
	}

	void Plugin::updateViewTarget() {
		cvarManager->log("updating view target");
		if (!gameWrapper->IsInReplay()) {
			return;
		}
		auto camera = gameWrapper->GetCamera();
		NULL_CHECK(camera);

		//get the steam id of the player targeted
		auto pri = PriWrapper(reinterpret_cast<std::uintptr_t>(camera.GetViewTarget().PRI));
		NULL_CHECK(pri);

		this->gazeData = this->data->getPlayerGazeData(pri.GetUniqueId().ID);
		this->playbackIndex = 0;
	}

	void Plugin::Render(CanvasWrapper canvas) {
		//only draw when we are in a replay
		if (!gameWrapper->IsInReplay() || gameWrapper->IsPaused()) {
			return;
		}

		auto replayEvent = gameWrapper->GetGameEventAsReplay();
		NULL_CHECK(replayEvent);

		//have to apply this stupid offset because i dont know proper start time
		float elapsed = replayEvent.GetReplayTimeElapsed() + REPLAY_OFFSET;

		auto camera = gameWrapper->GetCamera();
		NULL_CHECK(camera);

		//only render if in player view and we have data on that player
		const std::string camstate = camera.GetCameraState();
		if (camstate != CAMERA_STATE_BALL_CAM && camstate != CAMERA_STATE_CAR_CAM) {
			return;
		}

		if (this->gazeData.size() == 0) {
			return;
		}

		//Find the nearest eye point.

		//Reset iter to beginning if we rewound
		if (playbackIndex != 0) {
			auto lastPointTime = gazeData[playbackIndex-1].useconds;
			if (lastPointTime > elapsed) {
				cvarManager->log("rewinding");
				//we rewinded, need to search back for nearest time
				//for now just resetting it to beginning. dont want to deal with it
				this->playbackIndex = 0;
			}
		} else {
			cvarManager->log("at beginning");
		}

		bool shouldRender = false;
		for (; playbackIndex < this->gazeData.size(); playbackIndex++) {
			//TODO: need a limit on how close it needs to be
			//TODO: make sure we arent missing any frames
			if (gazeData[playbackIndex].useconds >= elapsed) {
				shouldRender = true;
				break;
			}
		}

		if (!shouldRender) {
			return;
		}
		auto point = gazeData[playbackIndex];

		Vector2 canvasSize = canvas.GetSize();
		int x = point.x * canvasSize.X;
		int y = point.y * canvasSize.Y;
		canvas.SetPosition(Vector2{ x - 7, y - 7 });
		canvas.SetColor(255, 0, 0, 150);
		canvas.DrawBox(Vector2{ 15, 15 });
	}


	//=======================
	//Tobii Device methods
	//=======================

	void Plugin::initTobii() {
		//custom log so that it logs to the game dev console
		tobii_custom_log_t custom_log{
			this,
			[](void* context, tobii_log_level_t level, char const* text) {
				Plugin* self = static_cast<Plugin*>(context);
				self->cvarManager->log(text);
			}
		};

		if (isTobiiError(tobii_api_create(&this->tobii, NULL, &custom_log), "Could not init tobii api.")) {
			return;
		}

		this->refreshTobiiDevices();
		if (this->tobiiDeviceURLs.size() == 0) {
			cvarManager->log("Could not find any tobii devices!");
			return;
		}
		this->connectTobiiDevice(this->tobiiDeviceURLs[0]);
	}

	void Plugin::refreshTobiiDevices() {
		this->tobiiDeviceURLs.clear();

		//Enumerate devices to find connected eye trackers
		auto err = tobii_enumerate_local_device_urls(this->tobii, [](const char* url, void* ctx) {
			auto urls = (std::vector<std::string>*) ctx;
			urls->push_back(url);
		}, &this->tobiiDeviceURLs);
		isTobiiError(err, "Could not refresh devices.");
	}

	void Plugin::connectTobiiDevice(std::string url) {
		cvarManager->log("Connecting to tobii device at url:" + url);
		isTobiiError(tobii_device_create(this->tobii, url.c_str(), TOBII_FIELD_OF_USE_ANALYTICAL, &this->tobiiDevice), "Could not connect to tobii device.");
	}

	void Plugin::disconnectTobiiDevice() {
		if (this->tobiiDevice != nullptr) {
			cvarManager->log("Disconnecting from current tobii device");
			if (isTobiiError(tobii_device_destroy(this->tobiiDevice), "Could not disconnect from tobii device.")) {
				return;
			}
			this->tobiiDevice = nullptr;
		}
	}

	void Plugin::pollTobiiData() {
		if (!this->recording) {
			return;
		}
		if (isTobiiError(tobii_device_process_callbacks(this->tobiiDevice), "Failed to poll for tobii data")) {
			//if error, go ahead and stop recording and disconnect
			stopRecording();
			disconnectTobiiDevice();
		}
	}

	bool Plugin::isTobiiError(tobii_error_t err, std::string msg) {
		if (err != TOBII_ERROR_NO_ERROR) {
			if (!msg.empty()) {
				this->cvarManager->log(msg);
			}

			this->cvarManager->log("[Tobii Library Error] " + std::string(tobii_error_message(err)));
			return true;
		}
		return false;
	}
}